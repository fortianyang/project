#include <iostream>
#include<unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include<sys/time.h>
#include<sys/resource.h>
#include<signal.h>

void handler(int signo)
{
    std::cout<<"signo : "<<signo<<std::endl;
}
int main()
{
    // 资源不足导致os终止进程，是通过信号终止的
    for(int i;i<=31;i++)
    {
        signal(i,handler);
    }
    // //限制累计运行时长
    // struct rlimit r;
    // r.rlim_cur=1;
    // r.rlim_max=RLIM_INFINITY;
    // setrlimit(RLIMIT_CPU,&r);
    // while (1) ;


    //限制使用的空间大小
    struct rlimit r;
    r.rlim_cur=1024*1024*40;//20M
    r.rlim_max=RLIM_INFINITY;
    setrlimit(RLIMIT_AS,&r);
    int count=0;
    while(true)
    {
        int *p=new int[1024*1024];
        count++;
        std::cout<<"size: "<<count<<std::endl;
        sleep(1);
    }
    return 0;
}