#include <boost/algorithm/string.hpp>

#include <iostream>
#include <vector>

int main()
{
    std::vector<std::string> tokens;
    const std::string str = "1 判断回文数 简单 30000";
    const std::string sep = " ";
    // boost::split(tokens,str,boost::is_any_of(sep),boost::algorithm::token_compress_off);
    boost::split(tokens, str, boost::is_any_of(sep), boost::algorithm::token_compress_on);
    for (auto &iter : tokens)
    {
        std::cout << iter << std::endl;
    }
    return 0;
}