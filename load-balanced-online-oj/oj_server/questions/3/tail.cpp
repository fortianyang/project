#ifndef COMPILER_ONLINE
#include "header.cpp"
#endif

void Test1()
{
    vector<int> num = {1, 9, 8, 4, 5, 4, 4, 4, 4};
    int ret = Solution().MoreThanHalfNum_Solution(num);
    if (ret == 4)
    {
        std::cout << "通过用例1, 测试[1, 9, 8, 4, 5, 4, 4, 4, 4]通过 ... OK!" << std::endl;
    }
    else
    {
        std::cout << "没有通过用例1, 测试的值是: [1, 9, 8, 4, 5, 4, 4, 4, 4]" << std::endl;
    }
}

void Test2()
{
    vector<int> num = {1, 1, 1, 1, 1, 1, 1, 1};
    int ret = Solution().MoreThanHalfNum_Solution(num);
    if (ret == 1)
    {
        std::cout << "通过用例1, 测试[1, 1, 1, 1, 1, 1, 1, 1]通过 ... OK!" << std::endl;
    }
    else
    {
        std::cout << "没有通过用例1, 测试的值是: [1, 1, 1, 1, 1, 1, 1, 1]" << std::endl;
    }
}

int main()
{
    Test1();
    Test2();

    return 0;
}
