#ifndef COMPILER_ONLINE
#include "header.cpp"
#endif

void Test1()
{
    vector<int> num = {1, 2, 3, 4, 4, 5, 6, 1};
    int ret = Solution().duplicate(num);
    if (ret==1||ret==4)
    {
        std::cout << "通过用例1, 测试[1, 2, 3, 4, 4, 5, 6, 1]通过 ... OK!" << std::endl;
    }
    else
    {
        std::cout << "没有通过用例1, 测试的值是: [1, 2, 3, 4, 4, 5, 6, 1]" << std::endl;
    }
}

void Test2()
{
    vector<int> num = {1, 1, 1, 1, 1, 1, 1, 1};
    int ret = Solution().duplicate(num);
     if (ret==1)
    {
        std::cout << "通过用例1, 测试[1, 1, 1, 1, 1, 1, 1, 1]通过 ... OK!" << std::endl;
    }
    else
    {
        std::cout << "没有通过用例1, 测试的值是: [1, 1, 1, 1, 1, 1, 1, 1]" << std::endl;
    }
}

int main()
{
    Test1();
    Test2();

    return 0;
}

// 最终提交给后台编译运行的代码header.cpp 和tail.cpp部分